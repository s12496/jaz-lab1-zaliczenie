package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("<title>JAZ - Lab1 - Formularz kredytowy</title>\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("<h3>Formularz kredytowy</h3>\n");
      out.write("<form action=\"kalkulator-kredytowy\" method=\"post\">\n");
      out.write("\t<label><div>Kwota kredytu:</div><div><input type=\"number\" id=\"kwota_kredytu\" name=\"kwota_kredytu\" value=\"10000\" step=\"500\" min=\"1000\" max=\"250000\"/></div></label>\n");
      out.write("\t<div></div>\n");
      out.write("\t<label><div>Ilość rat:</div><div><input type=\"number\" id=\"ilosc_rat\" name=\"ilosc_rat\"  value=\"144\" step=\"6\" min=\"6\" max=\"360\"/></div></label>\n");
      out.write("\t<div></div>\n");
      out.write("\t<label><div>Oprocentowanie:</div><div><input type=\"number\" id=\"oprocentowanie\" name=\"oprocentowanie\" value=\"3.5\" min=\"3.5\" max=\"18.0\" step=\"0.1\"/></div></label>\n");
      out.write("\t<div></div>\n");
      out.write("\t<label><div>Opłata stała:</div><div><input type=\"number\" id=\"oplata_stala\" name=\"oplata_stala\" value=\"5.00\" min=\"0.00\" max=\"25.00\" step=\"0.50\"/></div></label>\n");
      out.write("\t<div></div>\n");
      out.write("\t<label><div>Rodzaj rat:</div><div><input type=\"radio\" id=\"rodzaj_rat\" name=\"rodzaj_rat\" value=\"malejąca\" checked/>malejąca<input type=\"radio\" id=\"rodzaj_rat\" name=\"rodzaj_rat\" value=\"stała\"/>stała</div></label>\n");
      out.write("\t</br></br>\n");
      out.write("\t<label><div></div><div><input type=\"submit\" value=\"Wylicz ratę\"/></div></label>\n");
      out.write("</form>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
