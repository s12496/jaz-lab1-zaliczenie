package servlets;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/kalkulator-kredytowy")
public class KalkulatorKredytowy extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		float kwotaKredytu = Float.parseFloat(req.getParameter("kwota_kredytu"));
		int iloscRat = Integer.parseInt(req.getParameter("ilosc_rat"));
		float oprocentowanie = Float.parseFloat(req.getParameter("oprocentowanie"));
		float oplataStala = Float.parseFloat(req.getParameter("oplata_stala"));
		String rodzajRat = req.getParameter("rodzaj_rat");
		
		if(kwotaKredytu==0 || iloscRat==0 || oprocentowanie==0 || oplataStala<0) {
			res.sendRedirect("/");
		}
				
		res.setContentType("text/html");
		res.setCharacterEncoding("UTF-8");
		
		res.getWriter().println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n"
				+ "<html>\n"
				+ "<head>\n"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
				+ "<title>JAZ - Lab1 - Harmonogram spłat - rata " + rodzajRat + "</title>\n"
				+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/>\n"
				+ "</head>\n"
				+ "<body>\n");
		
		res.getWriter().println("<h1>Harmonogram spłat - rata " + rodzajRat + ".</h1>\n");

		res.getWriter().println("<h3>Kwota kredytu: " + kwotaKredytu + ", Ilość rat: " + iloscRat + ", Oprocentowanie: " + oprocentowanie + "</h3>\n");

//			res.getWriter().println(rodzajRat);	
			
			float[][] rataMalejaca = obliczMalejaca(kwotaKredytu, iloscRat, oprocentowanie, oplataStala);
			float[][] rataStala = obliczStala(kwotaKredytu, iloscRat, oprocentowanie, oplataStala);
			
		
		// Rysujemy tabelke

		res.getWriter().println("<table border=1>"
				+ "<thead><tr><th>Nr raty</th><th>Część kapitałowa</th><th>Część odsetkowa</th><th>Opłata stała</th><th>Rata miesięczna</th><th>Pozostała kwota spłaty</th></tr></thead>"
				+ "<tbody>");

		if (rodzajRat.equals("malejąca")) {
		
			for (int n=1; n <= iloscRat; n++) {
			
				res.getWriter().println("<tr>"
						+ "<td>" + (n) + "</td>"
						+ "<td>" + rataMalejaca[n][0] + "</td>"
						+ "<td>" + rataMalejaca[n][1] + "</td>"
						+ "<td>" + oplataStala + "</td>"
						+ "<td>" + rataMalejaca[n][2] + "</td>"
						+ "<td>" + rataMalejaca[n][3] + "</td>"
						+ "</tr>");

			}
			
		}
			
			else {
				for (int n=1; n <= iloscRat; n++) {
					
					res.getWriter().println("<tr>"
							+ "<td>" + (n) + "</td>"
							+ "<td>" + rataStala[n][0] + "</td>"
							+ "<td>" + rataStala[n][1] + "</td>"
							+ "<td>" + oplataStala + "</td>"
							+ "<td>" + rataStala[n][2] + "</td>"
							+ "<td>" + rataStala[n][3] + "</td>"
							+ "</tr>");

				}
				
			}
		
		
		res.getWriter().println("</tbody></table>");
		
		res.getWriter().println("</body>"
				+ "</html>");
				
	}


float[][] obliczMalejaca (float P, float N, float r, float os) {
// P - kwota kredytu, N - ilość rat, r - oprocentowanie, os - oplata stala

	float i = r / 100 / 12;	// kapitalizacja odsetek miesieczna
	
	float[][] harmonogram = new float[(int) (N+1)][4];
	
	for (int k=1; k <= N; k++) {		// Iteracja tyle razy, ile jest rat
		
		// Tablica ma format: [Nr raty][kapital, odsetki, rata, pozostala kwota splaty]
		
		harmonogram[k][0] = (float) (Math.round(P / N * 100.0) / 100.0);								// czesc kapitalowa
		harmonogram[k][1] = (float) (Math.round(P / N * i * (N - k + 1) * 100.0) / 100.0);				// odsetki zawarte w racie Rk
		harmonogram[k][2] = (float) (Math.round(P / N * ( 1 + i * (N - k + 1)) * 100.0) / 100.0) + os;	// wysokosc k-tej raty
		harmonogram[k][3] = (float) (Math.round(P * (1 - (k / N)) * 100.0) / 100.0);					// saldo po k-tej racie
	
		}
	
	return harmonogram;

	}

float[][] obliczStala (float P, float N, float r, float os) {
	// P - kwota kredytu, N - ilość rat, r - oprocentowanie, os - oplata stala

		float kwotaPozostala = P;
		float i = r / 100 / 12;	// kapitalizacja odsetek miesieczna
		
		float a = ((1 / i) * (1 - (float)(Math.pow((1 + i),-N))));
		float R = (float) (Math.round((P / a) * 100.0) / 100.0);
		
		float[][] harmonogram = new float[(int) (N+1)][4];
		
		for (int k=1; k <= N; k++) {		// Iteracja tyle razy, ile jest rat
			
			// Tablica ma format: [Nr raty][kapital, odsetki, rata, pozostala kwota splaty]
			
			harmonogram[k][0] = (float) (Math.round(R * Math.pow((1 + i),-1 * (N-k+1)) * 100.0) / 100.0);			// Tk - czesc kapitalowa
			harmonogram[k][1] = (float) (Math.round(R * (1 - (Math.pow((1 + i),-1 * (N-k+1)))) * 100.0) / 100.0);	// odsetki zawarte w racie Rk
			harmonogram[k][2] = (float) (Math.round((P / a) * 100.0) / 100.0) + os;									// wysokosc k-tej raty
			kwotaPozostala = kwotaPozostala - harmonogram[k][0];
			harmonogram[k][3] = (float) (Math.round(kwotaPozostala * 100.0) / 100.0);								// saldo po k-tej racie
		
			}
		
		return harmonogram;

		}

}