<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JAZ - Lab1 - Formularz kredytowy</title>
<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<h3>Formularz kredytowy</h3>
<form action="kalkulator-kredytowy" method="post">
	<label><div>Kwota kredytu:</div><div><input type="number" id="kwota_kredytu" name="kwota_kredytu" value="10000" step="500" min="1000" max="250000"/></div></label>
	<div></div>
	<label><div>Ilość rat:</div><div><input type="number" id="ilosc_rat" name="ilosc_rat"  value="144" step="6" min="6" max="360"/></div></label>
	<div></div>
	<label><div>Oprocentowanie:</div><div><input type="number" id="oprocentowanie" name="oprocentowanie" value="3.5" min="3.5" max="18.0" step="0.1"/></div></label>
	<div></div>
	<label><div>Opłata stała:</div><div><input type="number" id="oplata_stala" name="oplata_stala" value="5.00" min="0.00" max="25.00" step="0.50"/></div></label>
	<div></div>
	<label><div>Rodzaj rat:</div><div><input type="radio" id="rodzaj_rat" name="rodzaj_rat" value="malejąca" checked/>malejąca<input type="radio" id="rodzaj_rat" name="rodzaj_rat" value="stała"/>stała</div></label>
	</br></br>
	<label><div></div><div><input type="submit" value="Wylicz ratę"/></div></label>
</form>
</body>
</html>